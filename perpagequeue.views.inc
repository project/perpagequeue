<?php
/**
 * @file Per Page Queue's Views hooks.
 */

/**
 * Implements hook_views_api().
 */
function perpagequeue_views_api() {
  return array(
    'api' => 3,
  );
}
