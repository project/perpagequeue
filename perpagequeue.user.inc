<?php
/**
 * @file Per Page Queue's user hooks.
 */

/**
 * Implements hook_permission().
 */
function perpagequeue_permission() {
  return array(
    'manage page subqueue' => array(
      'title' => t('Create a Per Page Subqueue'),
      'description' => t('Manage all Per Page subqueues.'),
    ),
  );
}
