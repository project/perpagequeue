<?php
/**
 * @file Per Page Queue's menu hooks.
 */

/**
 * Implements hook_menu().
 */
function perpagequeue_menu() {
  $items = array();
  
  $items['admin/structure/nodequeue/%nodequeue/path'] = array(
  	'title' => 'Active Paths',
    'page callback' => 'perpagequeue_admin_subqueues',
    'page arguments' => array(3),
    'access callback' => 'perpagequeue_manage_path_access',
    'access arguments' => array(3),
    'file' => 'pages/admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  
  $items['admin/structure/nodequeue/%nodequeue/path/%subqueue/delete'] = array(
  	'title' => 'Delete Path',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('perpagequeue_admin_delete_subqueue', 3, 5),
    'file' => 'pages/admin.inc',
    'type' => MENU_CALLBACK,
  );
  
  $items['admin/structure/nodequeue/%nodequeue/path/add'] = array(
  	'title' => 'Add a New Path',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('perpagequeue_admin_add_subqueue', 3),
    'file' => 'pages/admin.inc',
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}
