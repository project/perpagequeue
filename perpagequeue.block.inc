<?php
/**
 * @file Per Page Queue's block hooks.
 */

/**
 * Implements hook_block_info().
 */
function perpagequeue_block_info() {
  $blocks = array();
  
  foreach (nodequeue_load_queues(nodequeue_get_all_qids()) as $qid => $queue) {
    if ($queue->owner == 'perpagequeue') {
      
      // Administrative block
      $blocks[$queue->name . '@admin'] = array(
        'info' => t('Manage Per Page Queues for !queue', array('!queue' => $queue->title)),
        'cache' => DRUPAL_CACHE_PER_PAGE,
        'properties' => array(
          'administrative' => TRUE,
        ),
      );
      
      // Display block
      if (module_exists('views') && substr(views_api_version(), 0, 1) >= 3) {
        if ($queue->views_view != -1) {
          list($view_id, $display_id) = explode('@', $queue->views_view, 2);
          $view = views_get_view($view_id);
          $blocks[$queue->name . '@view'] = array(
            'info' => t('Per Page Queue Views block for !queue', array('!queue' => $queue->title)),
            'cache' => DRUPAL_CACHE_PER_PAGE,
          );
        }
      }
      
    }
  }
  
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function perpagequeue_block_view($delta = '') {  
  list($queue_name, $block_type) = explode('@', $delta, 2);
  
  if (($queue = nodequeue_load_queue_by_name($queue_name))) {
    
    switch ($block_type) {
      case 'admin':
        return perpagequeue_block_view_admin($queue);
        
      case 'view':
        return perpagequeue_block_view_view($queue);
    }
  }
  
  return array();
}

/**
 * Block builder for the Per Page Queue admin block.
 */
function perpagequeue_block_view_admin($queue) {
  $block = array();
  
  if (perpagequeue_manage_path_access($queue)) {
    
    $block['subject'] = check_plain($queue->title);
    $block['content'] = array();
    
    if ($queue->use_tokens) {
      $all_paths = _perpagequeue_path_permutations();
    }
    else {
      $all_paths = array(drupal_get_path_alias());
    }
    
    $subqueues = _perpagequeue_order_subqueues_by_paths(_perpagequeue_load_subqueues_by_paths($queue, $all_paths), $all_paths);
    
    $path_options = array_flip($all_paths);
    foreach ($subqueues as $subqueue) {
      unset($path_options[$subqueue->reference]);
    }
    $path_options = array_flip($path_options);
    $block['content'] = drupal_get_form('perpagequeue_block_form__' . $queue->name, $queue, $subqueues, $path_options, $all_paths);
  }
  
  return $block;
}

/**
 * Block builder for the Per Page Queue View block.
 */
function perpagequeue_block_view_view($queue) {
  $block = array();
  
  if ($queue->views_view != -1) {
    if (($subqueue = perpagequeue_load_subqueue_by_path($queue))) {
      list($view_id, $display_id) = explode('@', $queue->views_view, 2);
      
      $view = views_get_view($view_id);
      $block['content'] = $view->preview($display_id, array($subqueue->sqid));
      
      $substitutions = $view->build_info['substitutions'];
      $substitutions['%1'] = $subqueue->title;
      $block['subject'] = strtr($view->build_info['title'], $substitutions);
    }
  }
  
  return $block;
}

/**
 * Form builder for the manage per page queue block form.
 */
function perpagequeue_block_form($form, &$form_state, $queue, $subqueues, $path_options, $all_paths) {
  $form = array();
  
  $form['queue'] = array(
    '#type' => 'value',
    '#value' => $queue,
  );
  
  if (!empty($path_options)) {
    $form['paths'] = array(
      '#type' => 'value',
      '#value' => $path_options,
    );
    
    if ($queue->use_tokens) {
      $form['path'] = array(
        '#type' => 'select',
        '#title' => t('Choose path reference'),
        '#description' => t('Choose which path reference to use when creating the new subqueue. The placeholder (%) will catch any text between two slashes.'),
        '#options' => $path_options,
      );
    }
    else {
      $form['path'] = array(
        '#type' => 'value',
        '#value' => '0',
      );
    }
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add subqueue for this page'),
    );
  }
  
  if (!empty($subqueues)) {
    $subqueue = array_shift($subqueues);
    
    $form['active_path'] = array(
      '#prefix' => '<div class="active-path">',
      '#markup' => t('The path <code>!path</code> is currently active on this page.', array('!path' => l($subqueue->reference, 'admin/structure/nodequeue/' . $queue->qid . '/view/' . $subqueue->sqid))),
      '#suffix' => '</div>',
    );
    
    if (!empty($subqueues)) {
      $items = array();
      foreach ($subqueues as $subqueue) {
        $items[] = l($subqueue->reference, 'admin/structure/nodequeue/' . $queue->qid . '/view/' . $subqueue->sqid);
      }
      
      $form['additional_paths'] = array(
        '#prefix' => '<div class="additional-paths">' . format_plural(
          count($items),
          'The following path is also active:',
          'The following paths are also active:'),
        '#markup' => theme('item_list', array('items' => $items)),
        '#suffix' => t('but will only display on this page if the above path is deleted.') . '</div>',
      );
    }
    
    $form['manage_paths'] = array(
      '#prefix' => '<div class="manage-paths">',
      '#markup' => l(t('Manage paths'), 'admin/structure/nodequeue/' . $queue->qid . '/path'),
      '#suffix' => '</div>',
    );
    
  }
  
  return $form;
}

/**
 * Form submit handler for perpagequeue_block_form.
 */
function perpagequeue_block_form_submit($form, &$form_state) {
  $queue = $form_state['values']['queue'];
  $path = $form_state['values']['paths'][$form_state['values']['path']];
  
  nodequeue_add_subqueue($queue, $path, $path);
  
  $form_state['redirect'] = 'admin/structure/nodequeue/' . $queue->qid . '/path';
}
