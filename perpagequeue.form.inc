<?php
/**
 * @file Per Page Queue's forms hook.
 */

/**
 * Implements hook_forms().
 */
function perpagequeue_forms($form_id, $args) {
  $forms = array();
  
  if (preg_match('/^perpagequeue_block_form__.+$/', $form_id)) {
    $forms[$form_id] = array(
      'callback' => 'perpagequeue_block_form',
    );
  }
  
  return $forms;
}
